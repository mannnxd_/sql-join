Soal 1 Mengambil Data dari Database

a. Mengambil data users
select id, name, email from users;


b. Mengambil data items
- Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
select * from items where price>1000000;

- Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
select * from items where name like 'uniklo%';

c. Menampilkan data item join dengan kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;

Soal 2 Mengubah Data dari Database

Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000. Masukkan query pada text jawaban di nomor ke 2.

update items set price=2500000 where id=1;